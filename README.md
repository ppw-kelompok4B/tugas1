# Tugas 1 PPW Kelompok 4 - Kelas B

## Anggota Kelompok
1. Abraham Williams Lumbantobing - 1706039414
2. Darin Amanda Zakiya - 1706979190
3. Kevin Raikhan Zain - 1706075041
4. Natasya Meidiana Akhda - 1706979392

## Status Aplikasi
[![Pipeline](https://gitlab.com/ppw-kelompok4B/tugas1/badges/master/pipeline.svg)](https://gitlab.com/ppw-kelompok4B/tugas1/commits/master)
[![Coverage](https://gitlab.com/ppw-kelompok4B/tugas1/badges/master/coverage.svg)](https://gitlab.com/ppw-kelompok4B/tugas1/commits/master)

## Link Heroku App
https://sinarperak.herokuapp.com/