from django.apps import AppConfig


class DonationformConfig(AppConfig):
    name = 'donationform'
