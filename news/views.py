from django.shortcuts import render
from django.http import HttpResponse
from .models import News1
from programs.models import Program

def index(request, namaberita):
	data = News1.objects.all()
	dataPrograms = Program.objects.all()
	sesuatu = []

	# Buat masukin semua objek dengan link palu ke list sesuatu
	for i in dataPrograms:
		if i.link == namaberita:
			sesuatu.append(i)

	indeks = 0
	nextnews = 0

	for i in range(len(data)):
		if(data[i].link.lower() == namaberita.lower()):
			indeks = i
	if indeks == len(data) - 1:
		nextnews = 0
	else:
		nextnews = indeks + 1

	response = {'data' : data[indeks], 'nextnews' : data[nextnews], 'sesuatu' : sesuatu}

	return render(request, 'news.html', response)
