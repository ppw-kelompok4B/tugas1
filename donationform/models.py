from django.db import models
from django.contrib.auth.models import User
from programs.models import Program

# Create your models here.
class Donation(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    program = models.ForeignKey(Program, on_delete=models.CASCADE, null=True, blank=True)
    # program = models.CharField(max_length=300, default='')
    # firstname = models.CharField(max_length=300, default='Anonymous')
    # lastname = models.CharField(max_length=300, default='Anon')
    # email = models.CharField(max_length=300, default='anon@mail.com')
    amount = models.DecimalField(decimal_places=1, max_digits=20, default=0)
    # showname = models.BooleanField(default=True)

    def __str__(self):
        if (self.user == None):
            return "Anonymous"


        return self.user.username + ' untuk ' + self.program.title

