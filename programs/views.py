from django.shortcuts import render
from .models import Program


# Create your views here.
def programs(request):
    program = Program.objects.all()
    return render(request, 'programs/programs.html', {'program': program})