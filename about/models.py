from django.db import models

# Create your models here.
class all_comments(models.Model):
    name = models.CharField(max_length=50)
    comment = models.TextField(max_length = 350)
def __str__(self):
    return self.comment
