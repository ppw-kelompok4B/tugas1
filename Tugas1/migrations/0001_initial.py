# Generated by Django 2.1.1 on 2018-10-17 16:52

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LandingNews',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('landtitle', models.CharField(max_length=100)),
                ('sum1', models.CharField(max_length=500)),
                ('sum2', models.CharField(max_length=2000)),
                ('nextnews', models.CharField(max_length=200)),
                ('link', models.CharField(max_length=100)),
            ],
        ),
    ]
