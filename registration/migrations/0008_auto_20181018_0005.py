# Generated by Django 2.1.1 on 2018-10-17 17:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0007_auto_20181018_0003'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registrationback',
            name='birth_date',
            field=models.DateField(null=True),
        ),
    ]
