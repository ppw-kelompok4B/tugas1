from django.test import TestCase, Client
from django.urls import resolve
from .models import News1
from . import views

class NewsTest(TestCase):
    def test_using_news_func(self):
        found = resolve('/news/*/')
        self.assertEqual(found.func, views.index)

    def test_model_can_create_new_activity(self):
        new_activity = News1.objects.create()
        all_activities = News1.objects.all().count()
        self.assertEqual(all_activities, 1)
