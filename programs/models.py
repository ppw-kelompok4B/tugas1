from django.db import models

# Create your models here.
class Program(models.Model):
    kode= models.CharField(max_length=1)
    title = models.CharField(max_length=20)
    content = models.CharField(max_length=100)
    # imgsrc = models.ImageField(upload_to="static/programs/img/", default="static/programs/img/banner.jpg")
    imgsrc= models.CharField(max_length=100)
    link = models.CharField(max_length=100)
