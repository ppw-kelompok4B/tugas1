from django.contrib import admin

# Register your models here.
from .models import RegistrationBack

admin.site.register(RegistrationBack)