# Generated by Django 2.1.1 on 2018-10-18 03:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Tugas1', '0002_landingnews_imgsrc'),
    ]

    operations = [
        migrations.AddField(
            model_name='landingnews',
            name='nextlink',
            field=models.CharField(default='#', max_length=100),
            preserve_default=False,
        ),
    ]
