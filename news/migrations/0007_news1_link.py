# Generated by Django 2.1.1 on 2018-10-18 03:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0006_auto_20181017_2050'),
    ]

    operations = [
        migrations.AddField(
            model_name='news1',
            name='link',
            field=models.CharField(default='', max_length=1000),
        ),
    ]
