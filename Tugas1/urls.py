from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from . import views

app_name = 'Tugas1'

urlpatterns = [
        path('', views.index, name='index'),
	path('index', views.index, name='index'),
        path('landingnews/<str:namaberita>',views.landing, name='namaberita'),
        path('logout', views.logout_view , name='logout'),
]
