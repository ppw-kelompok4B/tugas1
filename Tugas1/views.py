from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.core import serializers
from django.contrib.auth import authenticate, login, logout
from .models import LandingNews
import requests
import json


# Create your views here.
def index(request):
    return render(request, 'index.html', {'landing':landing})

def register(request):
    return render(request, 'register.html')

def programs(request):
    return render(request, 'programs.html')

def about(request):
    return render(request, 'about.html')

def logout_view(request):
	request.session.flush()
	logout(request)
	return HttpResponseRedirect('/index')

def landing(request,namaberita):
    data = LandingNews.objects.all()
	# for i in data:
	# 	if i.title.lower() == namaberita.lower():
    
    indeks = 0
    nextnews = 0
    
    for i in range(len(data)):
        if(data[i].link.lower() == namaberita.lower()):
            
            indeks = i
            
    if indeks == len(data) - 1:
        nextnews = 0
    else:
        nextnews = indeks + 1
    
    response = {'data' : data[indeks], 'nextnews' : data[nextnews]}
    return render(request, 'landingnews.html', response)
