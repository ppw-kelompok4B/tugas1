from django import forms
from .models import RegistrationBack

class Registration_Form(forms.Form):
    front_name = forms.CharField(label = '', max_length = 50, required=True, widget=forms.TextInput(
        attrs =  {'class': 'form-control', 'placeholder': 'Front Name'}))
    last_name = forms.CharField(label = '', max_length = 50, required=True, widget=forms.TextInput(
        attrs = {'class': 'form-control', 'placeholder': 'Last Name'}))
    email = forms.EmailField(label = '', max_length = 254, required = True, widget=forms.EmailInput(
        attrs = {'class': 'form-control', 'placeholder': 'john@mail.com'}))
    email_confirmation = forms.EmailField(label = '', max_length = 254, required = True, widget=forms.EmailInput(
        attrs = {'class': 'form-control', 'placeholder': 'john@mail.com'}))
    password = forms.CharField(label = '', max_length=32, required = True, widget=forms.PasswordInput(
        attrs = {'class': 'form-control', 'placeholder': 'Enter Password'}))
    password_confirmation = forms.CharField(label = '', max_length=32, required = True, widget=forms.PasswordInput(
        attrs = {'class': 'form-control', 'placeholder': 'Re-enter Password'}))
    birth_date = forms.DateField(label = '', required = True, widget = forms.SelectDateWidget(years=range(1950, 2018)))
    # ini selectdatewidget supaya kepisah-pisah gitu gak jadi satu

    def clean(self):
        cleaned_data = super().clean()
        emails = RegistrationBack.objects.all()
        email = cleaned_data.get("email")

        for i in emails:
            if i.email == email:
                raise forms.ValidationError(
                "Test"
            )
