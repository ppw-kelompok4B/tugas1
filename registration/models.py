from django.db import models

# Create your models here.
class RegistrationBack(models.Model):
    front_name = models.CharField(max_length = 50)
    last_name = models.CharField(max_length = 50)
    email = models.EmailField(null = True, max_length = 254)
    password = models.CharField(max_length=32)
    birth_date = models.DateField(null = True, blank = True)