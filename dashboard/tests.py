from django.test import TestCase, Client
from django.urls import resolve
from . import views

# Create your tests here.
class DashTest(TestCase):
    def test_dashboard_url(self):
        client = Client()
        response = client.get('/dashboard/')
        self.assertEquals(response.status_code, 302) # Redirect kalo belom login, makanya 302

    def test_using_dash_func(self):
        found = resolve('/dashboard/')
        self.assertEqual(found.func, views.dash)