from django.http import HttpResponseRedirect
from django.shortcuts import render
from donationform.views import donation_list_service

# Create your views here.
def dash(request):
    # user = request.user

    if not request.user.is_authenticated:
        return HttpResponseRedirect('/auth/login/google-oauth2/')

    return render(request, 'dashboard.html', {'user' : request.user})

def getAllDonation(request):
    if request.method == 'GET':
        return donation_list_service(request)