$(document).ready(function () {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    // AJAX untuk jumlah
    $.ajax({
        type: 'GET',
        url: 'getalldonation/',
        headers: {
            "X-CSRFToken": csrftoken
        },
        success: function (data) {
            // alert(data.data[0].amount);
            var jumlahDonasi = 0;
            for (var i = 0; i < data.data.length; i++) {
                jumlahDonasi += data.data[i].amount;
                $("#jumlahDonasi").html('<h4>' + jumlahDonasi + '</h4>');
            }


        },
        error: function (sesuatu) {
            alert('error');
        }


    });

    // AJAX untuk table
    $('#table_donasi').DataTable({
        responsive: true,
        "ajax": {
            type: 'GET',
            url: 'getalldonation/',
            headers: {
                "X-CSRFToken": csrftoken
            },
        },

        "columns": [{data: "program_title"}, {data: "amount"}],


    });


});