from django.urls import path
from .views import *

app_name = 'registration'
urlpatterns = [
	path('', registrationview, name='regis'),
	path('success', regisberhasil, name='berhasil'),
]