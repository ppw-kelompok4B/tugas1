# Generated by Django 2.1.1 on 2018-10-17 15:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0002_auto_20181017_2243'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registrationback',
            name='email',
            field=models.EmailField(max_length=254, null=True),
        ),
    ]
