from django.test import TestCase, Client
from django.urls import resolve
from .models import Donation
from .forms import DonationForm
from django.contrib.auth.models import User

from . import views


# Create your tests here.
class DonationTest(TestCase):

    def test_web_service_url_exist(self):
        client = Client()

        response = client.get('/dashboard/getalldonation/')
        self.assertEquals(response.status_code, 200)

    def test_url_donation_exist(self):
        response = Client().get('/donation/*/')
        self.assertEqual(response.status_code, 200)
    #
    def test_url_donation_added_exist(self):
        response = Client().get('/donation/added/')
        self.assertEqual(response.status_code, 200)

    def test_using_form_func(self):
        found = resolve('/donation/*/')
        self.assertEqual(found.func, views.form)

    def test_form_html(self):
        response = Client().get('/donation/*/')
        self.assertTemplateUsed(response, 'donation.html')

    def test_using_donate_func(self):
        found = resolve('/donation/donation_added/')
        self.assertEqual(found.func, views.donate)


    def test_forms(self):
        form_data = {'something': 'something'}
        form = DonationForm(data=form_data)
        self.assertFalse(form.is_valid())


    def test_model_can_create_new_activity(self):
        new_activity = Donation.objects.create()
        all_activities = Donation.objects.all()
        self.assertEqual(len(all_activities[0].__str__()) > 1, True)
