from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
import unittest

from django.conf import settings
from django.http import HttpRequest
from importlib import import_module
from .views import *
from .models import *

# Create your tests here.
#Cek URLnya ada atau ga
class RegisterTest(TestCase):
    def test_about_url_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)
#Cek pake template landingpage.html ga
    def test_about_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

    def test_alltestis_url_is_exist(self):
        response = Client().get('/about/share-testi/')
        self.assertEqual(response.status_code, 200)
    
    def test_text(self):
        request = HttpRequest()
        response = aboutus(request)
        html_response = response.content.decode('utf8')
        self.assertIn('What They Say', html_response)
    
    def test_about_page_using_about_page_func(self):
        found = resolve('/about/share-testi/')
        self.assertEqual(found.func, testimoni)

#Cek manggil functionnya bener atau ga
    def test_about_page_using_about_page_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, aboutus)
    
    def test_share_testi_returns_correct_json(self):
        response = Client().get('/about/share-testi/')
        self.assertJSONEqual(str(response.content, encoding='utf8'),{"nothing to see": "this isn't happening"})

    def test_testimony_model_can_add_new(self):
        all_comments.objects.create(name='Test', comment='test123')
        testimony_counts_all_entries = all_comments.objects.all().count()
        self.assertEqual(testimony_counts_all_entries, 1)
    
    # def test_form_save(self):
    #     context = self.client.post('/aboutus/', data={'name' : 'test', 'comment' : 'test'})
    #     self.assertEqual(context.status_code, 404)
    
    # def test_self_func_name(self):
    #     all_comments.objects.create(name='Test', comment='test123')
    #     testi = all_comments.objects.get(name='Test')
    #     self.assertEqual(str(testi), testi.name)
    
    def setUp(self):
        settings.SESSION_ENGINE = "django.contrib.sessions.backends.file"
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key


    



