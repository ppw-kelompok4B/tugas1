from django.db import models

# Create your models here.

class LandingNews(models.Model):
    landtitle = models.CharField(max_length=100)
    sum1 = models.CharField(max_length=500)
    # imgsrc = models.ImageField(upload_to="static/programs/img/", default="static/programs/img/banner.jpg")
    sum2 = models.CharField(max_length=2000)
    nextnews = models.CharField(max_length=200)
    linkreadmore = models.CharField(max_length=100)
    imgsrc = models.CharField(max_length=100)
    nextlink = models.CharField(max_length=100)
    link = models.CharField(max_length=100)