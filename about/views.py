from django.http import HttpResponse, JsonResponse
from .forms import testiform
from .models import all_comments
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
import requests, json

response={}
def aboutus(request):
    make_comment= testiform(request.POST or None)
    show_comment = all_comments.objects.all()
    response = {
        "show_comment" : show_comment
    }
    response['make_comment'] = make_comment
    return render(request, "about.html", response)

@csrf_exempt
def testimoni(request):
    response = {}
    if (request.method == "POST"):
        print("halo")
        share_comment = request.POST.get('share')
        if request.user.is_authenticated:
            if 'name' not in request.session:
                print("haloaaaa")
                request.session['name'] = request.user.first_name + " " + request.user.last_name
        response['name'] = request.session['name']
        name = response['name']
        new_comment = all_comments.objects.create(name=name, comment=share_comment)
        response['comment'] = share_comment
        return HttpResponse(json.dumps(response), content_type="application/json")
    else:
        return HttpResponse(json.dumps({"nothing to see": "this isn't happening"}), content_type="application/json")
