from django.db import models

# Create your models here.
class News1(models.Model):
    content = models.TextField(default="content")
    title = models.TextField(default="title")
    link = models.CharField(default="", max_length = 1000)
    imgsrc = models.CharField(default = "empty", max_length = 1000)
