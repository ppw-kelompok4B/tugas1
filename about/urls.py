from django.urls import path
from .views import *

app_name = 'about'
urlpatterns = [
	path('', aboutus, name='about'),
	path('share-testi/', testimoni, name='share'),
]