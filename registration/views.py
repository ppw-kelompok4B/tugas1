from django.shortcuts import render, redirect
from .forms import Registration_Form
from .models import RegistrationBack

# Create your views here.
def registrationview(request):
    form = Registration_Form(request.POST or None)
    response = {}
    users = RegistrationBack.objects.all()
    response['form'] = form
    response['users'] = users
    if(request.method == 'POST' and form.is_valid()):
        front_name = request.POST.get('front_name')
        last_name = request.POST.get('last_name')
        email = request.POST.get('email')
        password = request.POST.get('password')
        birth_date = request.POST.get('birth_date')
        user = RegistrationBack(
            front_name = front_name,
            last_name = last_name,
            email = email,
            password = password,
            birth_date = birth_date,
        )
        user.full_clean()
        user.save()
        return render(request, 'registration_success.html', response)
    else:
        return render(request, 'registration_page.html', response)
def regisberhasil(request):
    return render(request, 'registration_success.html', {})
