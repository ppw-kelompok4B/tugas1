from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from .forms import DonationForm
from .models import Donation
from programs.models import Program
from registration.models import RegistrationBack

# Create your views here.

# Webservice yang return semua donasi yang pernah dilakukan oleh user
'''
Bentuknya : 

{"data": [{"program_title": "Palu", "amount": 3700}, {"program_title": "Palu", "amount": 3000},
          {"program_title": "Palu", "amount": 200}, {"program_title": "Palu", "amount": 3006}]}
'''

def donation_list_service(request):
    # return json semua donasi yang pernah dilakukan user
    #
    if not request.user.is_authenticated:
        return HttpResponse("<h1> Not Allowed <h1>")

    data_banyak = list(request.user.donation_set.all())

    data = [] # Bentuknya List
    jeson = {} # Dictionary
    for i in range(len(data_banyak)):
        masuk_ke_simpan = {'program_title': data_banyak[i].program.title, 'amount': int(data_banyak[i].amount)}
        data.insert(i, masuk_ke_simpan)

    jeson['data'] = data

    return JsonResponse(jeson, safe=False)


def form(request, namaprogram):
    form = DonationForm()
    data = Program.objects.all()

    masuk = Program.objects

    for i in data:
        if i.link.lower() == namaprogram.lower():
            masuk = i
            break

    response = {'form': form, 'program': masuk, 'user': request.user}
    return render(request, 'donation.html', response)


response = {}


def donate(request):
    user = request.user

    if request.method == 'POST':
        form = DonationForm(request.POST or None)
        if form.is_valid():
            response['program'] = Program.objects.filter(title=request.POST['program'])[0]
            response['amount'] = request.POST['amount']

            print(type(request.POST['program']))
            donation = Donation(user=user, program=response['program'], amount=response['amount'])
            donation.save()
            return render(request, 'donation_added.html', response)

    else:
        return render(response, 'donation_form_invalid.html')
