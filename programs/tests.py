from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Program
from .views import *

# Create your tests here.
class programstestcase(TestCase):
	def test_url_exist(self):
		response = Client().get('/programs/')
		self.assertEqual(response.status_code, 200)

	def test_function_caller_exist(self):
		found = resolve('/programs/')
		self.assertEqual(found.func, programs)

	def test_landingpage_containt(self):
		response = self.client.get('/programs/')
		html_response = response.content.decode('utf8')
		self.assertIn('Join the Movement.', html_response)

	def test_can_create_new_program(self):
		new_act = Program.objects.create(kode='A', title= 'testtitle',content='testcontent',imgsrc='../static/programs/img/thirteen-j-608785-unsplash.jpg', link='#')
		counting_all_available_status =  Program.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)
