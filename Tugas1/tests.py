from django.test import TestCase, Client
from django.urls import resolve
from . import views

class UnitTest(TestCase):

    def test_url_home_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_landing_html(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_url_programs_exist(self):
         response = Client().get('/programs/')
         self.assertEqual(response.status_code, 200)

    def test_url_about_exist(self):
         response = Client().get('/about/')
         self.assertEqual(response.status_code, 200)
         
    def test_about_html(self):
         response = Client().get('/about/')
         self.assertTemplateUsed(response, 'about.html')
